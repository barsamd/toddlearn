from django.shortcuts import render_to_response
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth import login as django_login, authenticate, logout as django_logout

from .admin import AuthenticationForm, RegistrationForm, UserContactForm

def main(request):
    """
        Main page.
    """
    form_reg = RegistrationForm()
    form_reg_contact = UserContactForm()
    form_log = AuthenticationForm()
    return render_to_response('main.html', locals(),
            context_instance=RequestContext(request))

def register(request):
    """
        User registration.
    """
    if request.method == 'POST':
        form_reg = RegistrationForm(data=request.POST)
        form_reg_contact = UserContactForm(data=request.POST)
        form_log = AuthenticationForm()

        if form_reg.is_valid() and form_reg_contact.is_valid():
            user_info = form_reg.save()
            user_reg = form_reg_contact.save(commit=False)
            user_reg.user = user_info
            user_reg.save()
            
            phone_number = request.POST.get('contact')
            # TODO: PARSE OUT '-' from phone number before using
            
            return render_to_response('login.html', locals(),
                    context_instance=RequestContext(request))
        else:
            return render_to_response('main.html', locals(),
                    context_instance=RequestContext(request))
    else:
        raise Http404

def login(request):
    """
        Login,
    """
    if request.method == 'POST':
        form_log = AuthenticationForm(data=request.POST)
        form_reg = RegistrationForm()
        form_reg_contact = UserContactForm()
        
        if form_log.is_valid():
            username = request.POST['username']
            password = request.POST['password']

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    django_login(request, user)
                    return HttpResponseRedirect('account/')
        
        return render_to_response('main.html', locals(), 
                context_instance=RequestContext(request))
    else:
        raise Http404

def logout(request):
    """
        Logout.
    """
    django_logout(request)
    form_reg = RegistrationForm()
    form_reg_contact = UserContactForm()
    form_log = AuthenticationForm()
    return render_to_response('main.html', locals(),
            context_instance=RequestContext(request))
