from django.db import models
from django.contrib.auth.models import User

class User_Contact(models.Model):
	user = models.ForeignKey(User)
	contact = models.CharField(max_length=20)
	
	def __unicode__(self):
		return self.user.username + ' - ' + self.contact
		
class Categories(models.Model):
    title = models.CharField(max_length=20, unique=True)
    
    def __unicode__(self):
        return self.title
        
class Age_Group(models.Model):
	title = models.CharField(max_length=20, unique=True)
	
	def __unicode__(self):
		return self.title
	
class Tips(models.Model):
    title = models.CharField(max_length=50)
    tip = models.CharField(max_length=500)
    age_group = models.ForeignKey(Age_Group)
    rating = models.IntegerField(default=0) 
    category = models.ForeignKey(Categories, to_field='title', null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title
        
class Tip_Owner(models.Model):
    '''
        The owner of a submitted tip.
    '''

    tip = models.ForeignKey(Tips)
    user = models.ForeignKey(User)
    
    def __unicode__(self):
        return self.user.username + ' - ' + self.tip.title

class Ratings(models.Model):
    '''
        A rating for a tip.
    '''

    tip = models.ForeignKey(Tips)
    rate = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.tip.title + ' - ' + self.rate
    

class Favorites(models.Model):
    '''
        Users favorite tip.
    '''

    tip = models.ForeignKey(Tips)
    user = models.ForeignKey(User)
    
    def __unicode__(self):
        return self.user.username + ' - ' + self.tip.title
    
class Rated(models.Model):
    '''
        Rated tips.
    '''
    
    tip = models.ForeignKey(Tips)
    user = models.ForeignKey(User)
    action = models.CharField(max_length=1)
    
class Settings(models.Model):
    '''
        Users settings.
    '''
    
    user = models.ForeignKey(User)
    childs_birthday = models.DateField(auto_now=True)
    categories = models.CharField(max_length=40, null=True)
    
    def __unicode__(self):
    	return self.user.username