from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^$', 'main.views.main'),
        url(r'^register$', 'main.views.register', name='register'),
        url(r'^login$', 'main.views.login', name='login'),
        url(r'^logout$', 'main.views.logout', name='logout'),
)
