from django.contrib import admin
from django import forms
from django.forms import extras
import datetime

from .models import *

class AuthenticationForm(forms.Form):
    '''
        Authenticate an account (Login)
    '''
    username = forms.CharField(
            widget=forms.TextInput(attrs={'placeholder':'Username', 'required':True}),
            label='Username'
    )

    password = forms.CharField(
            widget= forms.PasswordInput(attrs={'placeholder':'password', 'required':True}),
            label='Password'
    )

    class Meta:
    	model = User
        fields = ['username', 'password']


class RegistrationForm(forms.ModelForm):
    '''
        Register a new account.
    '''
    username = forms.CharField(
            widget = forms.TextInput(attrs={'placeholder':'Username', 'pattern':'\w+',
                'required':True}),
            label='Username'
    )

    password = forms.CharField(
            widget = forms.PasswordInput(attrs={'placeholder':'Password', 'required':True}),
            label='Password'
    )
    
    password_conf = forms.CharField(
            widget = forms.PasswordInput(attrs={'placeholder':'Confirm Password', 
                'required':True}),
            label='Confirm Password'
    )

    class Meta:
        model = User
        fields = ['username', 'password', 'password_conf']

    def clean(self):
        ''' 
            Verifies that the passwords entered match.
        '''
        user = super(RegistrationForm, self).clean()
        if 'password' in self.cleaned_data and 'password_conf' in self.cleaned_data:
            if self.cleaned_data['password'] != self.cleaned_data['password_conf']:
                raise forms.ValidationError('passwords do not match.')
        return self.cleaned_data

    def save(self, commit=True):
        '''
            Save the new user.
        '''
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user
        
class UserContactForm(forms.ModelForm):
	'''
		User contact form.
	'''
	contact = forms.CharField(
				widget=forms.TextInput(attrs={'placeholder':'Cell (ie. 987-654-3210)','pattern':r'^(\d{3})-(\d{3})-(\d{4})$'}),
				label='Cell Number'
	)
	class Meta:
		model = User_Contact
		fields = ['contact']


class TipsForm(forms.ModelForm):
	'''
		Tips form.
	'''
	title = forms.CharField(
            widget= forms.TextInput(attrs={'placeholder':'Tip Title'}),
            label='Tip Title'
    )
    
	tip = forms.CharField(
            widget= forms.Textarea(
            	attrs={'placeholder':'Tip description (to insert link, type: {link name>http://url}'}),
            label='Tip'
	)
	
	age_group = forms.ModelChoiceField(queryset=Age_Group.objects.all(), empty_label=None)
	categories = forms.ModelChoiceField(queryset=Categories.objects.all(), empty_label=None)
	
	class Meta:
		model = Tips
		fields = ['age_group', 'categories', 'title', 'tip']
		
		
class SettingsForm(forms.ModelForm):
	'''
		Settings form.
	'''
	year = datetime.date.today().year+1
	childs_birthday = forms.DateField(
						widget=extras.SelectDateWidget(
							years=range(year-5, year)
						)
	)
	categories = forms.ModelMultipleChoiceField(queryset=Categories.objects.all())
	
	class Meta:
		model = Settings
		fields = ['childs_birthday', 'categories']
        
# ====== Register tables on the admin site

class TipsAdmin(admin.ModelAdmin):
    '''
        Register Tips table
    '''
    class Meta:
        model = Tips

admin.site.register(Tips, TipsAdmin)


class Tip_OwnerAdmin(admin.ModelAdmin):
    '''
        Register Tip_Owner table
    '''
    class Meta:
        model = Tip_Owner

admin.site.register(Tip_Owner, Tip_OwnerAdmin)


class RatingsAdmin(admin.ModelAdmin):
    '''
        Register Ratings table
    '''
    class Meta:
        model = Ratings

admin.site.register(Ratings, RatingsAdmin)


class FavoritesAdmin(admin.ModelAdmin):
    '''
        Register Favorites table
    '''
    class Meta:
        model = Favorites

admin.site.register(Favorites, FavoritesAdmin)

class CategoriesAdmin(admin.ModelAdmin):
	'''
		Register Categories table
	'''
	class Meta:
		model = Categories
		
admin.site.register(Categories, CategoriesAdmin)

class SettingsAdmin(admin.ModelAdmin):
	'''
		Register Settings table
	'''
	class Meta:
		model = Settings
		
admin.site.register(Settings, SettingsAdmin)

class Age_GroupAdmin(admin.ModelAdmin):
	'''
		Register Age_Groups
	'''
	class Meta:
		model = Age_Group
		
admin.site.register(Age_Group, Age_GroupAdmin)

class User_ContactAdmin(admin.ModelAdmin):
	'''
		Register User_Contact
	'''
	class Meta:
		model = User_Contact
admin.site.register(User_Contact, User_ContactAdmin)