/*
 * Search through tip bodies and substitute link syntax for link.
 */
function linkLoader() {
	$(".tip_body").each(function() {
		syntaxReader(this);
	});
}

function settings() {
	$(".settings").fadeToggle();
}

function current_settings() {
	var parse = $("#parse").text();
	if (parse !== "You have no settings") {
		var corrected = "Birthday: ";
		var parsed = parse.split("|");
		
		for (var i = 0; i < parsed.length; ++i) {
			if (i == 0) {
				corrected += parsed[i] + "\nCategories: ";
			}
			else {
				corrected += parsed[i] + ', ';
			}
		}
		corrected = corrected.substring(0, corrected.length-2);
		var obj = $("#parse").text(corrected);
		obj.html(obj.html().replace(/\n/g,'<br/>'));
	}
}

/*
 * Handle parsing.
 */
function syntaxReader(content) {
	var $e = $(content);
	var curText = $e.text();
	var finalContent = "";
	
	var splitLeft = curText.split("{");
	splitLeft = splitLeft != null ? splitLeft : "";

	if (splitLeft.length > 1) {	
		for (var i = 0; i < splitLeft.length; ++i) {
			var splitRight = splitLeft[i].split("}");
			splitRight = splitRight != null ? splitRight : "";
	
			if (splitRight.length == 1) {
				finalContent += splitRight[0];
			}
			else if (splitRight.length > 1) {
				for (var j = 0; j < splitRight.length; ++j) {
					var splitCarrot = splitRight[j].split(">");
					splitCarrot = splitCarrot != null ? splitCarrot : "";
					
					if (splitCarrot.length == 1) {
						finalContent += splitCarrot[0];
					}
					else if (splitCarrot.length > 1) {
						finalContent += "<a href=\"" + splitCarrot[1] + "\">" + splitCarrot[0] + "</a>";	
					}
				}
			}
			
		}
		$e.html(finalContent);
	}
}

function dislike() {
	AJAXcall("/account/dislike");
}

function favorite() {
	AJAXcall("/account/favorite");
}

function like() {
	AJAXcall("/account/like");
}

/*
 * Handle control (like, dislike, fav) processos.
 */
function correctControls() {
	var tip = $(".title").attr("id");
	var rated = $("#rated").text();

	if (rated.length > 0) {
		var parsedIds = rated.split("-");
		for (var i = 0; i < parsedIds.length; ++i) {
			id = parsedIds[i].split("|");
			var id;
			if (id[0] == tip) {
				/* Keeping seperate incase the actions need to be customized */
				if (id[1] == "l") {
					$("#like").remove();
					$("#dislike").remove();
				}
				else if (id[1] == "d") {
					$("#like").remove();
					$("#dislike").remove();
				}
			}
		}	
	}
}

/*
 * Make a custom AJAX call.
 */
function AJAXcall(myURL) {
	var tipId = $(".tip").children(":first").attr("id");
	
	/* Insert CSRF token in AJAX header */
	$.ajaxSetup({
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}	
		}	
	});
	
	$.ajax({
		type: "POST",
		url: myURL,
		data: { "tipid": tipId },
		
		success: function() {
			$("#like").remove();
			$("#dislike").remove();
		},
		
		error: function() {
			alert("Ajax call error");
		}
	});
}

/*
 * Cookie handling for CSRF
 */
function getCookie(name) {
	var CookieValue = null;
	if (document.cookie && document.cookie != "") {
		var cookies = document.cookie.split(";");
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			
			if (cookie.substring(0, name.length + 1) == (name + "=")) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	
	return cookieValue;
}

var csrftoken = getCookie("csrftoken");

function csrfSafeMethod(method) {
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}