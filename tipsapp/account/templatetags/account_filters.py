from django import template
from django.db.models import Q
from itertools import chain
import random
from datetime import date

from main.models import *

register = template.Library()

@register.filter(name='get_user_tips')
def get_user_tips(user):
	'''
		Given the user, retrieve all users tips.
	'''
	users_tips = Tip_Owner.objects.filter(user=user).values_list('tip_id', flat=True).distinct()
	return Tips.objects.filter(pk__in=users_tips)

@register.assignment_tag
def totd(user):
	'''
		Get the tip of the day for the given user.
		Insure that the tip is not one of the users own tips.
		
		Also, if Settings are defined, filter properly.
	'''
	users_tips = Tip_Owner.objects.filter(user=user).values_list('tip_id', flat=True)
	users_favs = Favorites.objects.filter(user=user).values_list('tip_id', flat=True)
	
	invalid_list = list(chain(users_tips, users_favs))
	tips = Tips.objects.filter(~Q(id__in=invalid_list)).values_list('id', flat=True)
	
# 	Settings
	valid_list = []
	settings = Settings.objects.filter(user=user)
	if settings:
		settings = settings[0]
		cat_ids = settings.categories.split('|')
		age = cat_ids[-1:][0].split('-')
		cat_ids = cat_ids[:-1]
		for c in cat_ids:
			cat = Categories.objects.get(pk=c)
			val = Tips.objects.filter(category=cat)
			for v in val:
				valid_list.append(v.id)
		
		born = date(int(age[0]), int(age[1]), int(age[2]))
		today = date.today()
		val = []
		if (today.year - born.year - ((today.month, today.day) < (born.month, born.day))) < 2:
			val = Tips.objects.filter(age_group=Age_Group.objects.get(pk=1))
		else:
			val = Tips.objects.filter(age_group=Age_Group.objects.get(pk=2))
		for v in val:
			if valid_list.count(v.id) > 0:
				valid_list.remove(v.id)
		tips = tips.filter(id__in=valid_list)
	
	num = selecting_tip = int(random.random() * 1000) % len(tips) if len(tips) > 0 else None
	
	return Tips.objects.filter(pk=int(tips[num]))[0] if num != None else None
	
@register.filter(name='get_user_favorites')
def get_user_favorites(user):
	'''
		Given the user, retrieve all users favorites.
	'''
	users_favs = Favorites.objects.filter(user=user).values_list('tip_id', flat=True)
	return Tips.objects.filter(pk__in=users_favs)
	
@register.filter(name='rated')
def rated(user):
	'''
		Return a list of the users rated tips.
	'''
	rated_ids = Rated.objects.filter(user=user).values_list('tip_id', flat=True)
	rated = Rated.objects.filter(user=user)
	tips = Tips.objects.filter(pk__in=rated_ids)
	rlist = []
	for e, f in zip(rated, tips):
		rlist.append(str(f.id) + '|' + e.action)
	
	return '-'.join(rlist)
	
@register.filter(name='get_category')
def get_category(e):
	return Categories.objects.get(e.category).title
	
@register.filter(name='get_settings')
def get_settings(user):
	'''
		Return the users current settings.
	'''
	settings = Settings.objects.filter(user=user)
	string = ''
	if settings:
		settings = settings[0]
		string += str(settings.childs_birthday) + '|'
		for c in settings.categories.split('|')[:-1]:
			string += Categories.objects.get(pk=c).title + ', '
		string = string[:-2]
	else:
		string = 'You have no settings'
	return string
		
	
	
	
	
	