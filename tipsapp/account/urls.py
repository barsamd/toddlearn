from django.conf.urls import patterns, url

urlpatterns = patterns('',
        url(r'^$', 'account.views.main'),
        url(r'^addtip$', 'account.views.add_tip'),
        url(r'^like$', 'account.views.like'),
        url(r'^dislike$', 'account.views.dislike'),
        url(r'^favorite$', 'account.views.favorite'),
        url(r'^savesettings$', 'account.views.save_settings'),
)
