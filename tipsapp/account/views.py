from django.shortcuts import render_to_response
from django.http import HttpResponse, Http404
from django.template import RequestContext
from datetime import date

from main.admin import TipsForm, SettingsForm
from main.models import *

def main(request):
	'''
		Account main.
	'''
	tips_form = TipsForm()
	settings_form = SettingsForm()
	return render_to_response('account.html', locals(),
			context_instance=RequestContext(request))
			
			
def save_settings(request):
	'''
		Save users settings
	'''
	print request.POST
	if request.method == 'POST':
		settings_form = SettingsForm(data=request.POST)
		tips_form = TipsForm()
		
		if settings_form.is_valid():
			# remove old settings
			old = Settings.objects.filter(user=request.user)
			if len(old) > 0:
				for o in old:
					o.delete()
					
			# save new
			saved = settings_form.save(commit=False)
			saved.user = request.user
			saved.categories = ''
			
			categories = request.POST.getlist('categories')
			if len(categories) > 0:
				store = ''
				for c in categories:
					store += str(c) + '|'
					
				age_day = request.POST.get('childs_birthday_day')
				age_month = request.POST.get('childs_birthday_month')
				age_year = request.POST.get('childs_birthday_year')
				store += age_year + '-' + age_month + '-' + age_day
				saved.categories = store
				
			saved.save()
			
		return render_to_response('account.html', locals(),
			context_instance=RequestContext(request))
	else:
		raise Http404


def add_tip(request):
	'''
		Process new tip.
	'''
	if request.method == 'POST':
		tips_form = TipsForm(data=request.POST)
		settings_form = SettingsForm()
		if tips_form.is_valid():
			saved = tips_form.save(commit=False)
			saved.age_group = Age_Group.objects.get(pk=request.POST.get('age_group', None))
			saved.category = Categories.objects.get(pk=request.POST.get('categories', None))
			saved.save()
			
			# Add tip owner
			tip_owner = Tip_Owner()
			tip_owner.user = request.user
			tip_owner.tip = saved
			tip_owner.save()
			
			tips_form = TipsForm()
		return render_to_response('account.html', locals(),
			context_instance=RequestContext(request))
	else:
		raise Http404
		
def like(request):
	'''
		Increment a tips rating.
	'''
	if request.method == 'POST':
		tipid = request.POST.get('tipid', None)
		tip = Tips.objects.get(pk=tipid)
		tip.rating = tip.rating + 1
		tip.save()
		
		Rated(user=request.user, tip=tip, action='l').save()
	else:
		raise Http404
	return HttpResponse()
		
def dislike(request):
	'''
		Decrement a tips rating, if rating falls below threshold, remove.
	'''
	if request.method == 'POST':
		tipid = request.POST.get('tipid', None)
		tip = Tips.objects.get(pk=tipid)
		tip.rating = tip.rating - 1
		
		if tip.rating < tipRemoveThreshold():
			tip.delete()
		else:
			tip.save()
			Rated(user=request.user, tip=tip, action='d').save()
	else:
		raise Http404
	return HttpResponse()
			
def tipRemoveThreshold():
	user_count = len(User.objects.all())
	return -10 if user_count < 1000 else -(user_count / 1000 + 10)
	
def favorite(request):
	'''
		Make this tip the users favorite.
	'''
	if request.method == 'POST':
		tipid = request.POST.get('tipid', None)
		tip = Tips.objects.get(pk=tipid)
		fav = Favorites(tip=tip, user=request.user)
		fav.save()
		
		# also like this tip
		like(request)
	else:
		raise Http404
	return HttpResponse()